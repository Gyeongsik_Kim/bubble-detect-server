var express = require('express');
var app = express();
var request = require('request');
var utf8 = require('utf8');
var multer = require('multer');
var gcm = require('node-gcm');
var request = require('request');


var photo = multer({ storage:multer.diskStorage({
		destination: function (req, file, cb) {
			cb(null, 'photo/');
		},
		filename: function (req, file, cb) {
			cb(null, file.originalname);
		}
	})
});

app.use('/photo', express.static('photo'));

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

app.post('/', photo.single('files'), function(req, res){
	console.log((Date.now()/1000) - (req.body.timestamp));
	console.log(req.body);
	console.log(req.file.path);
	res.send('');
	if(req.body.type == "original"){
		send(req.file.path);
	}
});

function send(path){
	const { exec } = require('child_process');
	exec('python server.py' + path);
	var options = {
		uri: "https://android.googleapis.com/gcm/send",
		method: "POST",
		headers : {
		"Content-Type" : "application/json",
		"Authorization" : "key=AIzaSyBgY4NqE7zrIvJ-02W8tulBqe0ZuOQG528"
		},
		json: {
		"to" : "cfeRSyACDR4:APA91bFume6FveuSTEXZ39BgKrZYLRp7LGlghsiv0eWI-jRQbBWxAy4cHM4CMranZkIY1SkEvK2j_3a5S1RP2yBHFDmEAlT5F-zwGvnbeIBCpCdPaT3ED32qgaEZpwW8v6eed2SJSFmr",
		"content_available":true,
		"priority" : "high",
		"data":{
			"type": "detect",
			"time": Date.now(),
			"path": path
		}
		 }
	};

	request(options, function (error, response, body) {
		if (!error && response.statusCode == 200) {
		console.log(body); // Print the shortened url.
		}
	});
}
