#-*- coding: utf-8 -*-
import os, datetime, time, sys, cv2
from datetime import datetime, timedelta, timezone
import numpy as np

template = None
end_template = None
cap = None
size_x = None
cap_time = None
detect_time = None

def pattern(img_rgb):
	global end_template
	global template

	bubble_list = []
	#img_rgb = cv2.imread('test_resultw.jpg')
	img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
	w, h = template.shape[::-1]
	we, he = end_template.shape[::-1]

	res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
	res_end = cv2.matchTemplate(img_gray, end_template, cv2.TM_CCOEFF_NORMED)
	threshold = 0.5
	threshold_end = 0.6
	loc = np.where( res >= threshold)
	loc_end = np.where(res_end >= threshold_end)
	before = (0,0)
	temp_zip = zip(*loc_end[::-1])
	for pt in temp_zip:
		if pt[0] - before[0] > 20 :
			bubble_list.append(pt + ('end', ))
			cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0,255,0), 2)
		before = pt

	before = (0,0)
	for pt in zip(*loc[::-1]):
		if pt[0] - before[0] > 20:
			bubble_list.append(pt + ('start', ))
			cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)
		before = pt
	cv2.imwrite('res.jpg',img_rgb)
	bubble_list = sorted(bubble_list, key=lambda bubble: bubble[0])
	print("SAVE")
	return bubble_list

def bubble_fillter(bubble_list):
	result = []
	last_append = None
	before = (0, 0)
	for pt in bubble_list :
		if pt[0] - before[0] > 30 or pt[2] != last_append :
			print('row : ' + str(pt))
			if last_append != pt[2]:
				result.append(pt)
				last_append = pt[2]
		before = pt
	distance = detect_distance(result)
	if distance == None :
		return None
	print("=====DISTANCE=====")
	for dis in distance :
		print(str(dis))
	draw_distance(distance)
	return result

def detect_distance(bubble_list):
	result = []
	global size_x
	back = 't'
	'''거품별 거리를 측정하는 함수'''
	for idx in range(0, len(bubble_list)):
		print(idx)
		if back == bubble_list[idx][2] :
			return None
		back = bubble_list[idx][2]
		if idx < len(bubble_list) :
			print('in')
			if bubble_list[idx][2] == 'end':
				if idx == 0:
					dis = bubble_list[idx][0] / 100 * 0.3
					start = (0, bubble_list[idx][1])
					end = (bubble_list[idx][0], bubble_list[idx][1])
					result.append((dis, start, end))
					print("DIFF : " + str(bubble_list[idx][0]))
			else :
				if idx == len(bubble_list) - 1:
					dis = (size_x - bubble_list[idx][0]) / 100 * 0.3
					start = (bubble_list[idx][0], bubble_list[idx][1])
					end = (size_x, bubble_list[idx][1])
					result.append((dis, start, end))
					print("DIFF : " + str(size_x - bubble_list[idx][0]))
				else :
					dis = (bubble_list[idx + 1][0] - bubble_list[idx][0]) / 100 * 0.3
					start = (bubble_list[idx][0], bubble_list[idx][1])
					end =  (bubble_list[idx + 1][0], bubble_list[idx + 1][1])
					result.append((dis, start, end))
					print("DIFF : " + str((bubble_list[idx + 1][0] - bubble_list[idx][0]) / 100 * 0.3))

	return result

def draw_distance(distance_list):
	global timeout
	global maxLen
	img = cv2.imread('res.jpg', 1)
	catch = False
	maxLen = 0
	for distance in distance_list:
		x = int((distance[2][0] + distance[1][0]) / 2)
		y = int((distance[2][1] + distance[1][1]) / 2)
		dis = '{:.3f}'.format(distance[0])+"cm"
		print(dis)
		maxLen = max(dis, maxLen)
		cv2.line(img, distance[1], distance[2], (0,255,255), 1)
		cv2.putText(img, dis, (x, y), cv2.FONT_HERSHEY_SIMPLEX,  1, (0, 255, 255), thickness=1)
		catch = True
	cv2.imwrite('res.jpg', img)
	if catch == True :
		timeout = 3
		detect_bubble()

def test(img_path):
	global cap_time
	global detect_time
	global size_x
	global cap
	cap_time = datetime.now()
	img = cv2.imread('img_path', cv2.IMREAD_GRAYSCALE)
	#img = cv2.resize(img, (0,0), fx=0.3, fy=0.3)
	cv2.imwrite('test_original.jpg', img)
	height, size_x = img.shape[:2]
	original_img = cv2.imread('test_original.jpg', 1)
	laplacian = cv2.Laplacian(img,cv2.CV_64F)
	sobelx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=5)
	#sobely = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=5)
	cv2.imwrite('test_result64.jpg', laplacian)
	cv2.imwrite('test_resultx.jpg', sobelx)
	#ret, thr = cv2.threshold(sobelx, 200, 255, cv2.THRESH_BINARY)
	#cv2.imwrite('test_resultw.jpg', thr)
	#ret, thr = cv2.threshold(sobely, 200, 255, cv2.THRESH_BINARY)
	#cv2.imwrite('test_resulty.jpg', sobely)
	thr = cv2.imread('test_resultx.jpg', 1)
	bubbles = pattern(thr)
	print("Pattern : " + str(bubbles))
	bubbles = bubble_fillter(bubbles)
	detect_time = datetime.now()
	print('총 걸린 시간 : ' + str(detect_time - cap_time))
	os.system('cp res.jpg ' + img_path.replace('original', 'detect'))

if __name__ == "__main__":
    file_path = sys.argv[1]
    test(file_path)
